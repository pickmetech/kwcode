﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;

namespace kwcode
{
    public class KwHelper
    {
        /// <summary>
        /// sql类型转C#类型
        /// </summary>
        /// <param name="sqlType"></param>
        /// <returns></returns>
        public static string SqlTypeToCsharpType(string sqlType, bool isNull)
        {
            if (isNull)
            {
                switch (sqlType)
                {

                    case "bigint":
                        return "long?";
                    case "bit":
                        return "bool?";
                    case "datetime":
                        return "DateTime?";
                    case "time":
                        return "DateTime?";
                    case "smalldatetime":
                        return "DateTime?";
                    case "datetime2":
                        return "DateTime?";
                    case "datetimeoffset":
                        return "DateTime?";
                    case "date":
                        return "DateTime?";
                    case "decimal":
                        return "decimal?";
                    case "float":
                        return "double?";
                    case "int":
                        return "int?";
                    case "smallint":
                        return "short?";
                    case "money":
                        return "decimal?";
                    case "real":
                        return "float?";
                    case "smallmoney":
                        return "decimal?";
                    case "tinyint":
                        return "byte?";
                    case "binary":
                        return "byte[]";
                    case "char":
                        return "string";
                    case "image":
                        return "byte[]";
                    case "nchar":
                        return "string";
                    case "ntext":
                        return "string";
                    case "nvarchar":
                        return "string";
                    case "text":
                        return "string";
                    case "timestamp":
                        return "object";
                    case "udt"://自定义的数据类型
                        return "object";
                    case "uniqueidentifier":
                        return "Guid";
                    case "varbinary":
                        return "object";
                    case "varchar":
                        return "string";
                    case "variant":
                        return "object";
                    case "xml":
                        return "object";
                    default:
                        return "object";
                }
            }
            else
            {
                switch (sqlType)
                {
                    case "bigint":
                        return "long";
                    case "bit":
                        return "bool";
                    case "datetime":
                        return "DateTime";
                    case "time":
                        return "DateTime";
                    case "smalldatetime":
                        return "DateTime";
                    case "datetime2":
                        return "DateTime";
                    case "datetimeoffset":
                        return "DateTime";
                    case "date":
                        return "DateTime";
                    case "decimal":
                        return "decimal";
                    case "float":
                        return "double";
                    case "int":
                        return "int";
                    case "smallint":
                        return "short";
                    case "money":
                        return "decimal";
                    case "real":
                        return "float";
                    case "smallmoney":
                        return "decimal";
                    case "tinyint":
                        return "byte";
                    case "binary":
                        return "byte[]";
                    case "char":
                        return "string";
                    case "image":
                        return "byte[]";
                    case "nchar":
                        return "string";
                    case "ntext":
                        return "string";
                    case "nvarchar":
                        return "string";
                    case "text":
                        return "string";
                    case "timestamp":
                        return "object";
                    case "udt"://自定义的数据类型
                        return "object";
                    case "uniqueidentifier":
                        return "Guid";
                    case "varbinary":
                        return "object";
                    case "varchar":
                        return "string";
                    case "variant":
                        return "object";
                    case "xml":
                        return "object";
                    default:
                        return "object";
                }

            }
        }

        /// <summary>
        /// 读文件内容
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string Read(string fileName)
        {
            if (!Exists(fileName))
            {
                return null;
            }
            //将文件信息读入流中
            using (FileStream fs = new FileStream(fileName, FileMode.Open))
            {
                string content = new StreamReader(fs, UTF8Encoding.UTF8).ReadToEnd();
                return content;
            }
        }

        /// <summary>
        /// 写入文件
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        public static bool Write(string fileName, string content)
        {
            if (content == null)
            {
                return false;
            }
            string directoryName = fileName.Substring(0,fileName.LastIndexOf("\\"));
            if (!Directory.Exists(directoryName))
            {
                Directory.CreateDirectory(directoryName);
            }

            //将文件信息读入流中
            using (FileStream fs = new FileStream(fileName, FileMode.Create))
            {
                lock (fs)//锁住流
                {
                    if (!fs.CanWrite)
                    {
                        throw new System.Security.SecurityException("文件fileName=" + fileName + "是只读文件不能写入!");
                    }

                    byte[] buffer = Encoding.UTF8.GetBytes(content);
                    fs.Write(buffer, 0, buffer.Length);
                    return true;
                }
            }
        }


        /// <summary>
        /// 判断文件是否存在
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool Exists(string fileName)
        {
            if (fileName == null || fileName.Trim() == "")
            {
                return false;
            }

            if (File.Exists(fileName))
            {
                return true;
            }

            return false;
        }

    }

    public class TableColumn
    {
        public string Name { get; set; }
        public string DataType { get; set; }
        public string CNName { get; set; }
    }
}
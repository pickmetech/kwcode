import { Component, OnInit } from '@angular/core';
import { _HttpClient } from '@delon/theme';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { API } from 'src/app/shared/api';

@Component({
  selector: 'app-$tableName-view',
  templateUrl: './view.component.html',
})
export class $TableNameViewComponent implements OnInit {
  record: any = {};
  i: any;
  constructor(private modal: NzModalRef, public msgSrv: NzMessageService, public http: _HttpClient) {}

  /**
   * 初始化时获取
   */
  ngOnInit(): void {
    this.http.get(API.$TableNameGet, { id: this.record.id }).subscribe((res) => (this.i = res.result));
  }

  close() {
    this.modal.destroy();
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
// tslint:disable-next-line:ordered-imports
import { STColumn, STComponent, STChange, STData } from '@delon/abc/st';
import { SFSchema } from '@delon/form';
import { ModalHelper, _HttpClient } from '@delon/theme';
import { NzMessageService, NzModalService } from 'ng-zorro-antd';
import { API } from 'src/app/shared/api';
import { DatePipe } from '@angular/common';
import { $TableNameEditComponent } from './edit/edit.component';
import { $TableNameViewComponent } from './view/view.component';

@Component({
  selector: 'app-$tableName-page',
  templateUrl: './$tableName.component.html',
})

// $CNName
export class $TableNameComponent implements OnInit {
  // 构造函数
  constructor(
    private http: _HttpClient,
    private modal: ModalHelper,
    private msgSrv: NzMessageService,
    private cfmSrv: NzModalService,
    private datePipe: DatePipe,
  ) {}

  // 获取数据api
  url = API.$TableNameGetAll;
  // 查询表单
  searchSchema: SFSchema = {
    properties: {
      carNumber: {
        type: 'string',
        title: '车牌号',
      },
    },
  };
  // table 表字段
  @ViewChild('st', { static: false }) st: STComponent;
  columns: STColumn[] = [
    // 序号和选择框
    { title: '', type: 'no', width: 50 },
    { title: '', type: 'checkbox', index: 'id' },

    // 字段
    $foreach
    ${[NotMap(Id,CreatorUserId,LastModificationTime,LastModifierUserId,IsDeleted,DeleterUserId,DeletionTime,TenantId)]
    { title: '$TableColumnCNName', index: '$TableColumnName' },
    $}

    // 操作
    {
      title: '操作',
      width: 200,
      fixed: 'right',
      buttons: [
        {
          text: '编辑',
          icon: 'edit',
          type: 'static',
          component: $TableNameEditComponent,
          click: (record) => this.add(record),
        },
        {
          text: '删除',
          icon: 'delete',
          type: 'del',
          pop: {
            okType: 'danger',
          },
          click: (record) => this.delete(record.id),
        },
        {
          text: '查看',
          icon: 'search',
          type: 'modal',
          component: $TableNameViewComponent,
          click: (record) => this.detail(record),
        },
      ],
    },
  ];

  // 勾选数据
  selectedRows: STData[] = [];
  date() {
    return this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss');
  }

  /**
   * 初始化
   */
  ngOnInit() {}

  /**
   * 新建/修改
   * @param record 数据
   */
  add(record?) {
    this.modal.createStatic($TableNameEditComponent, { record: record ? record : { id: 0 } }).subscribe(() => this.st.reload());
  }

  /**
   * 查看
   * @param record 数据
   */
  detail(record) {
    this.modal.createStatic($TableNameViewComponent, { record }).subscribe(() => this.st.reload());
  }

  /**
   * 单删除
   * @param  id 列表id
   */
  delete(id) {
    this.http.delete(API.$TableNameDelete, { id }).subscribe((res) => {
      this.msgSrv.success('操作成功');
      this.st.reload();
    });
  }

  /**
   * 列表勾选
   * @param e 变化类型
   */
  change(e: STChange) {
    if (e.type === 'checkbox') {
      this.selectedRows = e.checkbox;
    }
  }

  /**
   * 批量删除提示
   */
  confirm() {
    this.cfmSrv.confirm({
      nzTitle: '提示',
      nzContent: '确定批量删除勾选数据？',
      nzOnOk: () => this.deleteBatch(),
    });
  }

  /**
   * 批量删除
   */
  deleteBatch() {
    this.http.delete(API.$TableNameDeleteBatch, { id: this.selectedRows.map((i) => i.id) }).subscribe((res) => {
      this.msgSrv.success('操作成功');
      this.st.reload();
      this.st.clearCheck();
    });
  }
}

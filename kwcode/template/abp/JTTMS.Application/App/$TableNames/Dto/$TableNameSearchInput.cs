﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace JTTMS.App.$TableNames.Dto
{
    /// <summary>
    /// 查询$CNName条件
    /// </summary>
    public class $TableNameSearchInput : PagedAndSortedResultRequestDto, IShouldNormalize
    {
        //[Display(Name = "名称")]
        //public virtual string Name { get; set; }

        /// <summary>
        /// 如果实现此接口，则在验证之后（并且在方法调用之前）调用Normalize方法。
        /// </summary>
        public void Normalize()
        {
            if (string.IsNullOrWhiteSpace(Sorting))
            {
                Sorting = "Id DESC";
            }
        }
    }
}

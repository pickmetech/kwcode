﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using AutoMapper;
using JTTMS.App.$TableNames.Dto;
using JTTMS.THEntity.$TableNames;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace JTTMS.App.$TableNames.Dto
{
    /// <summary>
    /// $CNName更新模型
    /// </summary>
    [AutoMapTo(typeof($TableName))]
    public class $TableNameEditInput : $TableNameCreateInput, IEntityDto
    {
        /// <summary>
        /// 自增长id
        /// </summary>
        [Display(Name = "Id")]
        [Required(ErrorMessage = "{0}不能为空")]
        public virtual int Id { get; set; }


    }

}
